Thousands of free data sets available out there, ready to be analyzed and visualized. You just need to know where to look.

Recently I realized [Gitlab API](https://docs.gitlab.com/ee/api/) is open to everybody and could be a really great source for some fun.
